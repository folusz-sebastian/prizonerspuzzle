package prisoners;

public class Game {
    private int redHatsInFrontOfFirstPrisoner;

    public int getRedHatsInFrontOfFirstPrisoner() {
        return redHatsInFrontOfFirstPrisoner;
    }

    public void setRedHatsInFrontOfFirstPrisoner(int redHatsInFrontOfFirstPrisoner) {
        this.redHatsInFrontOfFirstPrisoner = redHatsInFrontOfFirstPrisoner;
    }
}
