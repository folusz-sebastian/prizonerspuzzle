package prisoners;

public class Utils {
    public static boolean isEven(int number) {
        return number % 2 == 0;
    }
}
