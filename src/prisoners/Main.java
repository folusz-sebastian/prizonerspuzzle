package prisoners;

import java.util.ArrayList;

public class Main {
    private static final int NUMBER_OF_PRISONERS = 10;

    public static void main(String[] args) {
        ArrayList<Prisoner> prisoners = PrisonersGenerator.generatePrisoners(NUMBER_OF_PRISONERS);
        prisoners.forEach((Prisoner prisoner) -> System.out.print(
                prisoner.getHatColor().toString() + ", "
                )
        );

        System.out.println();

        prisoners.forEach((Prisoner prisoner) -> System.out.print(
                prisoner.choseHatColor(prisoners).toString() + ", "
                )
        );
    }
}
