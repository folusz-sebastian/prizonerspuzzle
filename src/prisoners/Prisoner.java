package prisoners;

import java.util.List;
import static prisoners.Utils.isEven;

public class Prisoner {
    private HatColor hatColor;
    private int myIndexInQueue;
    private int redHatsInFrontOfMe;
    private int redHatsBehindMe;
    private Game game;

    public Prisoner(HatColor hatColor, int myIndexInQueue, Game game) {
        this.hatColor = hatColor;
        this.myIndexInQueue = myIndexInQueue;
        this.game = game;
    }

    public HatColor choseHatColor(List<Prisoner> prisoners) {
        boolean firstPrisoner = myIndexInQueue == 0;

        countRedHatsInFrontOfMe(prisoners);
        if (firstPrisoner) {
            game.setRedHatsInFrontOfFirstPrisoner(redHatsInFrontOfMe);

            if (isEven(redHatsInFrontOfMe))
                return HatColor.blue;
            else
                return HatColor.red;
        } else {
            countRedHatsBehindMeWithoutFirst(prisoners);

            int redHatsInFrontOfFirstPrisoner = game.getRedHatsInFrontOfFirstPrisoner();
            if (isEven(redHatsInFrontOfFirstPrisoner) ==
                    isEven(redHatsInFrontOfMe - redHatsBehindMe))
                return HatColor.blue;
            else
                return HatColor.red;
        }
    }

    public void countRedHatsInFrontOfMe(List<Prisoner> prisoners) {
        redHatsInFrontOfMe = 0;
        for (int i = myIndexInQueue + 1; i < prisoners.size(); i++) {
            if (prisoners.get(i).hatColor == HatColor.red)
                redHatsInFrontOfMe++;
        }
    }

    public void countRedHatsBehindMeWithoutFirst(List<Prisoner> prisoners) {
        redHatsBehindMe = 0;
        for (int i = 1; i < myIndexInQueue; i++) {
            if (prisoners.get(i).hatColor == HatColor.red)
                redHatsBehindMe++;
        }
    }

    public HatColor getHatColor() {
        return hatColor;
    }
}
