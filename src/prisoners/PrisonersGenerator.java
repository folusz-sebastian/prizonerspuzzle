package prisoners;

import java.util.ArrayList;
import java.util.Random;

public class PrisonersGenerator {
    public static ArrayList<Prisoner> generatePrisoners(int numberOfPrisoners) {
        ArrayList<Prisoner> prisoners = new ArrayList<>();
        Game game = new Game();
        Random random = new Random();

        for (int indexInQueue = 0; indexInQueue < numberOfPrisoners; indexInQueue++) {
            boolean hatIsRed = random.nextBoolean();
            if (hatIsRed)
                prisoners.add(new Prisoner(HatColor.red, indexInQueue, game));
            else
                prisoners.add(new Prisoner(HatColor.blue, indexInQueue, game));
        }
        return prisoners;
    }
}
